# CF App with mysql-client

Sample CloudFoundry app containing `mysql-client` binary.
Can be used if you need to do mysql interactions not over a `cf ssh` tunnel but directly from the container, i.e. import big dumps.

## Usage

```
# adjust service name in manifest.yml, copy your resources in this root dir, then push
cf push
```

```
# ssh to the container and do your stuff
cf ssh mysql-client

MYSQL_CREDS=$(echo $VCAP_SERVICES | jq '.["mysql-database"][0].credentials')
MYSQL_HOST=$(echo $MYSQL_CREDS | jq -r .hostname)
MYSQL_PORT=$(echo $MYSQL_CREDS | jq -r .port)
MYSQL_USER=$(echo $MYSQL_CREDS | jq -r .username)
MYSQL_PASSWORD=$(echo $MYSQL_CREDS | jq -r .password)
MYSQL_DB=$(echo $MYSQL_CREDS | jq -r .database)

deps/0/bin/mysql --host $MYSQL_HOST --port $MYSQL_PORT --user $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DB

# for example import dump:
deps/0/bin/mysql -f --host $MYSQL_HOST --port $MYSQL_PORT --user $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DB < app/<resource-from-root-dir>.sql > migration.out 2>&1
```
